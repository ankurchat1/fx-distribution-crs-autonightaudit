﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace FXDistributionUtility.HttpClientUtility
{
    public static class HttpUtility
    {
        public async static Task<string> PostToAPI(string Url,object model)
        {
            string _Output = string.Empty;

            try
            {
                using (var client = new HttpClient())
                {
                    var Records = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");                    
                    var response = client.PostAsync(Url, Records).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        _Output = "Status Code :" + response.StatusCode;
                    }
                    else
                    {
                        Response bookingResponse = new Response();
                        var responseContent = response.Content;
                        var _pb = await responseContent.ReadAsStringAsync();
                        string _OutputConvertToString = _pb;
                        _Output = _OutputConvertToString;
                    }
                }
            }
            catch (Exception ex)
            {

                _Output = "Error:" + ex.Message.ToString();
            }
            return await Task.FromResult(_Output);
        }
    }

    public class Response
    {
        public string transactionId { get; set; }
        public string[] errorCode { get; set; }
        public string correlationId { get; set; }
    }
}
