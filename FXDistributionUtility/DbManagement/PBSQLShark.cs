﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Data.SqlClient;

namespace FXDistributionUtility.DbManagement
{
    public class PBSQLShark
    {
        private AOneDbAdapter _dataAdapter = null;
        private string LocalConnection = string.Empty;
        public PBSQLShark(string LocalConnection)
        {
            this.LocalConnection = LocalConnection;
            this.InitDataAdapter();

        }

        public StoredProcedureResult ExecuteStoreProcedure(string spName)
        {
            return this._dataAdapter.ExecuteStoreProcedure(spName);
        }

        public StoredProcedureResult ExecuteStoreProcedure(string spName, Dictionary<string, object> parameters)
        {
            return this._dataAdapter.ExecuteStoreProcedure(spName, parameters);
        }

        public StoredProcedureResult ExecuteStoreProcedure(string spName, List<DirectionalParameter> parameters)
        {
            return this._dataAdapter.ExecuteStoreProcedure(spName, parameters);
        }

        private void InitDataAdapter()
        {
            //string connectionString = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString;
            this._dataAdapter = new AOneDbAdapter(0, LocalConnection);
        }

        //public static WebConnector Session {
        //    get {
        //        if (HttpContext.Current.Session["Connector"] == null)
        //        {
        //            HttpContext.Current.Session["Connector"] = new WebConnector();
        //        }
        //        return (WebConnector)HttpContext.Current.Session["Connector"];
        //    }
        //}
    }

    public class AOneDbAdapter : IDataAdapter, ILocalDataAdapter, IAdapter
    {
        // Fields
        private string _connStr;
        private string _database;
        private DBType _dbType;
        private bool _IntegratedSecurity;
        private string _server;
        private int _userID;
        private string _webKey;
        private DbConnection conn;

        // Methods
        public AOneDbAdapter(DBType type, string connString)
        {
            this.conn = null;
            this._server = null;
            this._database = null;
            this._IntegratedSecurity = false;
            this._connStr = null;
            this._dbType = type;
            this._connStr = connString;
            this.conn = ConnectionFactory.CreateConnection(DBType.sqlserver, this._connStr);
            this.conn.Open();
        }

        public AOneDbAdapter(DBType type, string server, string database)
        {
            this.conn = null;
            this._server = null;
            this._database = null;
            this._IntegratedSecurity = false;
            this._connStr = null;
            this._dbType = type;
            this._server = server;
            this._database = database;
        }

        public void Close()
        {
            if (this.conn.State != ConnectionState.Closed)
            {
                this.conn.Close();
            }
        }

        public StoredProcedureResult ExecuteStoreProcedure(string spName)
        {
            return this.ExecuteStoreProcedure(spName, new List<DirectionalParameter>());
        }

        public StoredProcedureResult ExecuteStoreProcedure(string spName, Dictionary<string, object> parameters)
        {
            return this.ExecuteStoreProcedure(spName, AdapterUtil.ConvertDictionaryToList(parameters));
        }

        public StoredProcedureResult ExecuteStoreProcedure(string spName, List<DirectionalParameter> parameters)
        {
            lock (this)
            {
                return StoreProcedureExecuter.Execute(spName, parameters, this.conn);
            }
        }

        public void Open()
        {
            if (this.conn.State != ConnectionState.Open)
            {
                this.conn.Open();
            }
        }

        // Properties
        public int UserID {
            set {
                this._userID = value;
            }
        }

        public string WebKey {
            set {
                this._webKey = value;
            }
        }
    }

    public interface IDataAdapter : ILocalDataAdapter, IAdapter
    {
        // Methods
        new StoredProcedureResult ExecuteStoreProcedure(string spName);
        new StoredProcedureResult ExecuteStoreProcedure(string spName, Dictionary<string, object> parameters);
        new StoredProcedureResult ExecuteStoreProcedure(string spName, List<DirectionalParameter> parameters);
    }

    public interface ILocalDataAdapter : IAdapter
    {
        // Methods
        StoredProcedureResult ExecuteStoreProcedure(string spName);
        StoredProcedureResult ExecuteStoreProcedure(string spName, Dictionary<string, object> parameters);
        StoredProcedureResult ExecuteStoreProcedure(string spName, List<DirectionalParameter> parameters);
    }

    public interface IAdapter
    {
        // Methods
        void Close();
        void Open();
    }

    public enum DBType
    {
        sqlserver,
        oracle,
        mysql
    }

    [Serializable]
    public class StoredProcedureResult
    {
        // Fields
        public Dictionary<string, object> Output;
        public DataSet ResultSet;
        public object ReturnValue;
    }

    [Serializable]
    public class DirectionalParameter
    {
        // Fields
        public ParameterDirection Direction;
        public string Name;
        public int Size;
        public SqlDbType? Type;
        public object Value;

        // Methods
        public DirectionalParameter()
        {
        }

        public DirectionalParameter(string name, ParameterDirection direction)
        {
            this.Name = name;
            this.Direction = direction;
        }

        public DirectionalParameter(string name, object value, ParameterDirection direction)
        {
            this.Name = name;
            this.Value = value;
            this.Direction = direction;
        }

        public DirectionalParameter(string name, object value, ParameterDirection direction, SqlDbType? type)
        {
            this.Name = name;
            this.Value = value;
            this.Type = type;
            this.Direction = direction;
        }

        public DirectionalParameter(string name, object value, ParameterDirection direction, SqlDbType? type, int size)
        {
            this.Name = name;
            this.Value = value;
            this.Type = type;
            this.Direction = direction;
            this.Size = size;
        }
    }

    public class ConnectionFactory
    {
        // Methods
        public static DbConnection CreateConnection(DBType type, string connectionString)
        {
            switch (type)
            {
                case DBType.sqlserver:
                    return new SqlConnection(connectionString);

                case DBType.mysql:
                    return new SqlConnection(connectionString);
            }
            return null;
        }

        public static string CreateConnectionString(string serverName, string serverPort, string DBName)
        {
            return CreateConnectionString(null, serverName, serverPort, DBName, null, false, 0, null, null);
        }

        public static string CreateConnectionString(string serverType, string serverName, string serverPort, string DBName, string networkLibrary)
        {
            return CreateConnectionString(serverType, serverName, serverPort, DBName, networkLibrary, false, 0, null, null);
        }

        public static string CreateConnectionString(string serverType, string serverName, string serverPort, string DBName, string networkLibrary, bool encrypt, int timeout)
        {
            return CreateConnectionString(serverType, serverName, serverPort, DBName, networkLibrary, encrypt, timeout, null, null);
        }

        public static string CreateConnectionString(string serverType, string serverName, string serverPort, string DBName, string networkLibrary, bool encrypt, int timeout, string userName, string password)
        {
            StringBuilder builder = new StringBuilder("");
            if (timeout != 0)
            {
                builder.Append("Connect Timeout=");
                builder.Append(timeout);
                builder.Append(";");
            }
            if (encrypt)
            {
                builder.Append("Encrypt=True;");
            }
            builder.Append("Server=");
            if (serverType != null)
            {
                builder.Append(serverType);
                builder.Append(":");
            }
            builder.Append(serverName);
            if (serverPort != null)
            {
                builder.Append(", ");
                builder.Append(serverPort);
            }
            builder.Append("; Database=");
            builder.Append(DBName);
            builder.Append(";");
            if (networkLibrary != null)
            {
                builder.Append("Net=");
                builder.Append(networkLibrary);
                builder.Append(";");
            }
            if (!(string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password)))
            {
                builder.Append("user id=");
                builder.Append(userName);
                builder.Append("; pwd=");
                builder.Append(password);
            }
            else
            {
                builder.Append("Integrated Security=SSPI");
            }
            return builder.ToString();
        }
    }

    internal class AdapterUtil
    {
        // Methods
        public static List<DirectionalParameter> ConvertDictionaryToList(Dictionary<string, object> dic)
        {
            List<DirectionalParameter> list = new List<DirectionalParameter>();
            foreach (string str in dic.Keys)
            {
                list.Add(new DirectionalParameter(str, dic[str], ParameterDirection.Input));
            }
            return list;
        }
    }

    public class StoreProcedureExecuter
    {
        // Methods
        public static StoredProcedureResult Execute(string spName, List<DirectionalParameter> parameters, DbConnection conn)
        {
            return Execute(spName, parameters, null, conn);
        }

        public static StoredProcedureResult Execute(string spName, List<DirectionalParameter> parameters, int? timeout, DbConnection conn)
        {
            StoredProcedureResult result2;
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            try
            {
                StoredProcedureResult result = new StoredProcedureResult();
                List<string> list = new List<string>();
                DbCommand command = conn.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                if (timeout.HasValue)
                {
                    command.CommandTimeout = timeout.Value;
                }
                command.CommandText = spName;
                if (parameters != null)
                {
                    foreach (DirectionalParameter parameter in parameters)
                    {
                        object obj2 = DBNull.Value;
                        if ((parameter.Direction == ParameterDirection.Output) || (parameter.Direction == ParameterDirection.InputOutput))
                        {
                            list.Add(parameter.Name);
                        }
                        else if (parameter.Direction == ParameterDirection.ReturnValue)
                        {
                            continue;
                        }
                        if (parameter.Value != null)
                        {
                            obj2 = parameter.Value;
                        }
                        if (!parameter.Type.HasValue)
                        {
                            command.Parameters.Add(new SqlParameter(parameter.Name, obj2));
                        }
                        else
                        {
                            command.Parameters.Add(new SqlParameter(parameter.Name, parameter.Type.Value));
                            command.Parameters[parameter.Name].Value = obj2;
                        }
                        command.Parameters[parameter.Name].Direction = parameter.Direction;
                        command.Parameters[parameter.Name].Size = parameter.Size;
                    }
                }
                command.Parameters.Add(new SqlParameter("@ReturnValue", DbType.Object));
                command.Parameters["@ReturnValue"].Direction = ParameterDirection.ReturnValue;
                DataSet set = new DataSet();
                using (DbDataReader reader = command.ExecuteReader())
                {
                    DataTable table = null;
                    do
                    {
                        table = new DataTable();
                        table.Load(reader);
                        set.Tables.Add(table);
                    }
                    while (!reader.IsClosed);
                    result.ResultSet = set;
                    result.ReturnValue = command.Parameters["@ReturnValue"].Value;
                    result.Output = new Dictionary<string, object>();
                    foreach (string str in list)
                    {
                        result.Output.Add(str, command.Parameters[str].Value);
                    }
                }
                conn.Close();
                result2 = result;
            }
            catch (Exception)
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                throw;
            }
            return result2;
        }

        public static StoredProcedureResult ExecuteWithText(string spName, List<DirectionalParameter> parameters, int? timeout, DbConnection conn)
        {
            StoredProcedureResult result2;
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            try
            {
                StoredProcedureResult result = new StoredProcedureResult();
                List<string> list = new List<string>();
                DbCommand command = conn.CreateCommand();
                command.CommandType = CommandType.Text;
                if (timeout.HasValue)
                {
                    command.CommandTimeout = timeout.Value;
                }
                command.CommandText = spName;
                if (parameters != null)
                {
                    foreach (DirectionalParameter parameter in parameters)
                    {
                        object obj2 = DBNull.Value;
                        if ((parameter.Direction == ParameterDirection.Output) || (parameter.Direction == ParameterDirection.InputOutput))
                        {
                            list.Add(parameter.Name);
                        }
                        else if (parameter.Direction == ParameterDirection.ReturnValue)
                        {
                            continue;
                        }
                        if (parameter.Value != null)
                        {
                            obj2 = parameter.Value;
                        }
                        if (!parameter.Type.HasValue)
                        {
                            command.Parameters.Add(new SqlParameter(parameter.Name, obj2));
                        }
                        else
                        {
                            command.Parameters.Add(new SqlParameter(parameter.Name, parameter.Type.Value));
                            command.Parameters[parameter.Name].Value = obj2;
                        }
                        command.Parameters[parameter.Name].Direction = parameter.Direction;
                        command.Parameters[parameter.Name].Size = parameter.Size;
                    }
                }
                command.Parameters.Add(new SqlParameter("@ReturnValue", DbType.Object));
                command.Parameters["@ReturnValue"].Direction = ParameterDirection.ReturnValue;
                DataSet set = new DataSet();
                using (DbDataReader reader = command.ExecuteReader())
                {
                    DataTable table = null;
                    do
                    {
                        table = new DataTable();
                        table.Load(reader);
                        set.Tables.Add(table);
                    }
                    while (!reader.IsClosed);
                    result.ResultSet = set;
                    result.ReturnValue = command.Parameters["@ReturnValue"].Value;
                    result.Output = new Dictionary<string, object>();
                    foreach (string str in list)
                    {
                        result.Output.Add(str, command.Parameters[str].Value);
                    }
                }
                conn.Close();
                result2 = result;
            }
            catch (Exception)
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                throw;
            }
            return result2;
        }
    }
}
