﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

namespace FXDistributionUtility.DataTableUtilities
{
    public static class DataTableUtility
    {
        public static List<T> ToListOf<T>(this DataTable dt, bool tostring = false)
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            var columnNames = dt.Columns.Cast<DataColumn>()
                .Select(c => c.ColumnName)
                .ToList();
            var objectProperties = typeof(T).GetProperties(flags);
            var targetList = dt.AsEnumerable().Select(dataRow =>
            {
                var instanceOfT = Activator.CreateInstance<T>();

                foreach (var properties in objectProperties.Where(properties => columnNames.Contains(properties.Name) && dataRow[properties.Name] != DBNull.Value))
                {
                    if (tostring)
                    {
                        properties.SetValue(instanceOfT, dataRow[properties.Name].ToString(), null);
                    }
                    else
                    {
                        properties.SetValue(instanceOfT, dataRow[properties.Name], null);
                    }
                }
                return instanceOfT;
            }).ToList();

            return targetList;
        }

        public static DataSet convertXmlMessageToDataSet(string message)
        {
            DataSet dataSet = new DataSet();
            System.IO.StringReader xmlSR = new System.IO.StringReader(message);
            XmlTextReader textReader = new XmlTextReader(xmlSR);
            dataSet.ReadXml(textReader);
            return dataSet;
        }
    }
}
