﻿using FXDistributionUtility.DbManagement;
using FXDistribution.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using FXDistribution.Models.InterfaceModel.SCTH.Hotelposition;
using FXDistribution.Models.CacheModel;
using FXDistributionUtility.HttpClientUtility;
using System.Text.Json;

namespace FXDistributionDatabaseUtility
{
    public class FXDistributionAccounting
    {
        private PBSQLShark _DbShark;
        

        public FXDistributionAccounting(string _ConnectionString)
        {            
            _DbShark = new PBSQLShark(_ConnectionString);
        }

        public async Task<string> UpdateAccoutingDate(AccountDateRequest _updateAccountDateRequest, long _propertyId)
        {
            string _Output = string.Empty;           
                    
            try
            {
                DataTable dt = new DataTable();
                var Dict = new Dictionary<string, object>();
                Dict.Add("@PmsCustId",_updateAccountDateRequest.PmsCustId);
                Dict.Add("@Accountingdate", _updateAccountDateRequest.AccountingDate);
                Dict.Add("@Modifydate", _updateAccountDateRequest.ReceivedDateTime);
                Dict.Add("@Source", _updateAccountDateRequest.Source);
                var _DataSet = _DbShark.ExecuteStoreProcedure("Sp_Post_AccountingDate", Dict).ResultSet;                 
                _Output = Convert.ToString(_DataSet.Tables[0].Rows[0][0]);
                _DataSet = new DataSet();
                
                

                var _Connection = await GetPropertyConnection(_propertyId, 10001);
                _DbShark = new PBSQLShark(_Connection);

                dt = new DataTable();
                Dict = new Dictionary<string, object>();
                Dict.Add("@PMSCustCode", Convert.ToInt32( _updateAccountDateRequest.PmsCustId));
                Dict.Add("@AccountingDate", _updateAccountDateRequest.AccountingDate);               
                 _DataSet = _DbShark.ExecuteStoreProcedure("FXCRS_Save_NightAuditDate", Dict).ResultSet;
                _Output = Convert.ToString(_DataSet.Tables[0].Rows[0][0]);


            }
            catch (Exception ex)
            {                

            }
            return await Task.FromResult(_Output);
        }


        public async Task<string> UpdateNightAudit(NightAuditRequestSarover Model)
        {
            string _Output = string.Empty;
            try
            {
                DataTable dt = new DataTable();
                var Dict = new Dictionary<string, object>();               
                DataSet _DataSet = new DataSet();
                //var _Connection = await GetPropertyConnection(Model.PropertyId, 10001);
                //_DbShark = new PBSQLShark(_Connection);
                dt = new DataTable();
                Dict = new Dictionary<string, object>();
                Dict.Add("@PMSCustCode", Convert.ToInt32(Model.PmsCustCode));
                Dict.Add("@AccountingDate", Model.AccountingDate);
                _DataSet = _DbShark.ExecuteStoreProcedure("FXFD_NightAuditDate", Dict).ResultSet;
                _Output = Convert.ToString(_DataSet.Tables[0].Rows[0][0]);


            }
            catch (Exception ex)
            {

            }
            return await Task.FromResult(_Output);
        }


        public async Task<string> UpdateFXOnePropertyDetails(FxOnePropertyRequest _updateFxOnePropertyRequest)
        {
            string _Output = string.Empty;

            try
            {
                DataTable dt = new DataTable();
                var Dict = new Dictionary<string, object>();
                Dict.Add("@SavePriority", _updateFxOnePropertyRequest.SavePriority);
                Dict.Add("@IsFXFOMEnabled", _updateFxOnePropertyRequest.IsFXFOMEnabled);
                Dict.Add("@IsFXCRSEnabled", _updateFxOnePropertyRequest.IsFXCRSEnabled);
                Dict.Add("@PropertyID", _updateFxOnePropertyRequest.PropertyID);
                Dict.Add("@PropertyCode", _updateFxOnePropertyRequest.PropertyCode);
                Dict.Add("@GroupCode", _updateFxOnePropertyRequest.GroupCode);
                Dict.Add("@ProductCode", _updateFxOnePropertyRequest.ProductCode);
                Dict.Add("@UserID", _updateFxOnePropertyRequest.UserID);
                var _DataSet = _DbShark.ExecuteStoreProcedure("Sp_Post_FxOnePropertyDetails", Dict).ResultSet;
                _Output = Convert.ToString(_DataSet.Tables[0].Rows[0][0]);
                _DataSet = new DataSet();

            }
            catch (Exception ex)
            {

            }
            return await Task.FromResult(_Output);
        }      



        public async Task<string> GetPropertyConnection(long PropertyID, int ProductCode)
        {
            string conString = string.Empty;
            
            #region Prepare ConnectionModel  for Property Connection String  
            var _ConnectionModel = new
            {
                User = "prithu",
                PropertyID = PropertyID,
                ProductCode = ProductCode
            };
          
            var _PropertyConnectionString = await HttpUtility.PostToAPI("https://fxauthenticationprod.azurewebsites.net/api/Connection/DbConnection", _ConnectionModel);

            var _ServerConnection = JsonSerializer.Deserialize<ServerConnectionModel>(_PropertyConnectionString);

            if (!string.IsNullOrEmpty(_ServerConnection.connectionString))
            {
                conString = _ServerConnection.connectionString;
            }
            else
            {
                conString = "Connection not found for the Property in FXONE,Please configure";
            }

            #endregion

            return await Task.FromResult(conString);
        }

    }

    public class ServerConnectionModel
    {
        public string connectionString { get; set; }
        public int Status { get; set; }
        public string message { get; set; }
    }
}
