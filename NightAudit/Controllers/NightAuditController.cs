﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace NightAudit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NightAuditController : ControllerBase
    {

       
//        private IMessage _Message { get; set; }

        //public NightAuditController(IMessage message)
        //{
        //    this._Message = message;
        //}

        [HttpPost]
        [Route("UpdateNightAuditAccountingDate")]
        public async Task<IActionResult> UpdateAccountingDate(NightAuditRequestSarover Model)
        {
            ResponseModel _Result = new ResponseModel();
            _Result.Message = "Success";
            _Result.Status = 1;            
            object Values = "";
            Model.AccountingDate = DateTime.Now.ToString("yyyy-MM-dd");
            //string DataBaseConnectionString = "Server=fxoneadmin.database.windows.net;Database=FX_Sabre_Interface_Log;User ID=fxoneadmin;Password=FXOneAdm@123;Trusted_Connection=False";

            string DataBaseConnectionString  = "Server = skyreslivedbserver.database.windows.net; Database = FXCRS_TESTDB; User ID = skyresadmin; Password = Sky@Res@123; Trusted_Connection = False";
            Message fxFXDistributionAccounting = new Message(DataBaseConnectionString);

            await fxFXDistributionAccounting.UpdateNightAudit(Model);
           
            return Ok(await Task.FromResult(_Result));
        }


        [HttpPost]
        [Route("CRSNightAuditAccountingDateUpdate")]
        public async Task<IActionResult> CRSNightAuditAccountingDateUpdate(NightAuditRequestSarover Model)
        {
            ResponseModel _Result = new ResponseModel();
            _Result.Message = "Success";
            _Result.Status = 1;
            object Values = "";
            Model.AccountingDate = DateTime.Now.ToString("yyyy-MM-dd");
            string DataBaseConnectionString = "Server=fxoneadmin.database.windows.net;Database=FX_Sabre_Interface_Log;User ID=fxoneadmin;Password=FXOneAdm@123;Trusted_Connection=False";

            //string DataBaseConnectionString = "Server = skyreslivedbserver.database.windows.net; Database = FXCRS_TESTDB; User ID = skyresadmin; Password = Sky@Res@123; Trusted_Connection = False";
            Message fxFXDistributionAccounting = new Message(DataBaseConnectionString);

            await fxFXDistributionAccounting.CRSNightAudit(Model);

            return Ok(await Task.FromResult(_Result));
        }


        [HttpPost]
        [Route("FXFDAutoAccoutingDateUpdate")]
        public async Task<IActionResult> FXFDAutoNightAudit(NightAuditRequestSarover Model)
        {
            ResponseModel _Result = new ResponseModel();
            _Result.Message = "Success";
            _Result.Status = 1;
            object Values = "";
            Model.AccountingDate = DateTime.Now.ToString("yyyy-MM-dd");
            string DataBaseConnectionString = "Server=fxoneadmin.database.windows.net;Database=FX_Sabre_Interface_Log;User ID=fxoneadmin;Password=FXOneAdm@123;Trusted_Connection=False";

            //string DataBaseConnectionString = "Server = skyreslivedbserver.database.windows.net; Database = FXCRS_TESTDB; User ID = skyresadmin; Password = Sky@Res@123; Trusted_Connection = False";
            Message fxFXDistributionAccounting = new Message(DataBaseConnectionString);

            await fxFXDistributionAccounting.FXFDAutoAccountingNightAudit(Model);

            return Ok(await Task.FromResult(_Result));
        }

    }
}