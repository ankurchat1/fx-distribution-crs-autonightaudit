﻿using FXDistributionUtility.DbManagement;
using FXDistributionUtility.HttpClientUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace NightAudit.Controllers
{
    public class Message : IMessage
    {

        private PBSQLShark _DbShark;


        public Message(string _ConnectionString)
        {
            _DbShark = new PBSQLShark(_ConnectionString);
        }

        public async Task<string> UpdateNightAudit(NightAuditRequestSarover Model)
        {
            string _Output = string.Empty;
            try
            {
                DataTable dt = new DataTable();
                var Dict = new Dictionary<string, object>();
                DataSet _DataSet = new DataSet();               
                dt = new DataTable();
                Dict = new Dictionary<string, object>();
                Dict.Add("@PMSCustCode", Convert.ToInt32(Model.PmsCustCode));
                Dict.Add("@AccountingDate", Model.AccountingDate);
                _DataSet = _DbShark.ExecuteStoreProcedure("FXFD_NightAuditDate", Dict).ResultSet;
                _Output = Convert.ToString(_DataSet.Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {

            }
            return await Task.FromResult(_Output);
        }


        public async Task<string> CRSNightAudit(NightAuditRequestSarover Model)
        {
            string _Output = string.Empty;
            try
            {
                DataTable dt = new DataTable();
                var Dict = new Dictionary<string, object>();
                DataSet _DataSet = new DataSet();
                //int _PropertyId = 40519;

                var _Connection = await GetPropertyConnection(Model.PropertyId, 10013);
                _DbShark = new PBSQLShark(_Connection);

                dt = new DataTable();
                Dict = new Dictionary<string, object>();
                Dict.Add("@PMSCustCode", Convert.ToInt32(Model.PmsCustCode));
                Dict.Add("@AccountingDate", Model.AccountingDate);
                _DataSet = _DbShark.ExecuteStoreProcedure("FXFD_NightAuditDate", Dict).ResultSet;
                _Output = Convert.ToString(_DataSet.Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {

            }
            return await Task.FromResult(_Output);
        }

        public async Task<string> FXFDAutoAccountingNightAudit(NightAuditRequestSarover Model)
        {
            string _Output = string.Empty;
            try
            {
                DataTable dt = new DataTable();
                var Dict = new Dictionary<string, object>();
                DataSet _DataSet = new DataSet();
                //int _PropertyId = 40519;

                var _Connection = await GetPropertyConnection(Model.PropertyId, 100);
                _DbShark = new PBSQLShark(_Connection);

                dt = new DataTable();
                Dict = new Dictionary<string, object>();
                Dict.Add("@PMSCustCode", Convert.ToInt32(Model.PmsCustCode));
                Dict.Add("@AccountingDate", Model.AccountingDate);
                _DataSet = _DbShark.ExecuteStoreProcedure("FXFD_NightAuditDate", Dict).ResultSet;
                _Output = Convert.ToString(_DataSet.Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {

            }
            return await Task.FromResult(_Output);
        }


        public async Task<string> GetPropertyConnection(long PropertyID, int ProductCode)
        {
            string conString = string.Empty;

            #region Prepare ConnectionModel  for Property Connection String  
            var _ConnectionModel = new
            {
                User = "prithu",
                PropertyID = PropertyID,
                ProductCode = ProductCode
            };

            var _PropertyConnectionString = await HttpUtility.PostToAPI("https://fxauthenticationprod.azurewebsites.net/api/Connection/DbConnection", _ConnectionModel);

            var _ServerConnection = JsonSerializer.Deserialize<ServerConnectionModel>(_PropertyConnectionString);

            if (!string.IsNullOrEmpty(_ServerConnection.connectionString))
            {
                conString = _ServerConnection.connectionString;
            }
            else
            {
                conString = "Connection not found for the Property in FXONE,Please configure";
            }

            #endregion

            return await Task.FromResult(conString);
        }

    }

    public class ServerConnectionModel
    {
        public string connectionString { get; set; }
        public int Status { get; set; }
        public string message { get; set; }
    }


    public class NightAuditRequestSarover
    { 
        public int PmsCustCode { get; set; }
        public string AccountingDate { get; set; }
        public int PropertyId { get; set; }
    }

    public class ResponseModel
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
