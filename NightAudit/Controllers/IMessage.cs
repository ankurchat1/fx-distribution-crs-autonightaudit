﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NightAudit.Controllers
{
    public interface IMessage
    {
        public Task<string> UpdateNightAudit(NightAuditRequestSarover Model);
    }
}
